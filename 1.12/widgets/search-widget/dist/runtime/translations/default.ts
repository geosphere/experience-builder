export default {
  _widgetLabel: 'Suchen',
  sketchLabel: 'Suchen',
  placeholderText: 'Orts- oder Adresssuche',
};
